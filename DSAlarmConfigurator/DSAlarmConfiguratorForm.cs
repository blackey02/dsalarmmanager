﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DSAlarmConfigurator
{
    public partial class DSAlarmConfiguratorForm : Form
    {
        private enum ObserverType { Adam, WebService }
        private ObserverType _observerType;
        private DateTime _statusDelayTimer = DateTime.MinValue;

        public DSAlarmConfiguratorForm()
        {
            InitializeComponent();
            DSAlarmShared.Log.Logger.Instance.Info("DSAlarmConfiguratorForm: Application started");
            this.MinimumSize = this.Size;
            radioButtonAdam.Checked = true;
            ShowStatus(false);
        }

        private void DSAlarmConfiguratorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            DSAlarmShared.Log.Logger.Instance.Info("DSAlarmConfiguratorForm_FormClosing: Application closing");
        }

        private async void ButtonGetCameras_Click(object sender, EventArgs e)
        {
            ShowStatus(true, "Loading cameras");
            checkedListBoxCameras.Items.Clear();
            await GetCamerasAsync();
            ShowStatus(false);
        }

        private async Task GetCamerasAsync()
        {
            var response = new DSAlarmShared.Pipe.Response();
            var request = new DSAlarmShared.Pipe.Request();
            request.Action = DSAlarmShared.Pipe.Request.PipeAction.GetCameras;
            request.Data = new DSAlarmShared.DS.System() { Ip = textBoxIp.Text, Port = ushort.Parse(textBoxPort.Text), Username = textBoxUsername.Text, Password = textBoxPassword.Text };

            List<DSAlarmShared.DS.Camera> cameras = null;
            using (var client = new DSAlarmShared.Pipe.Client(DSAlarmShared.Constants.PipeName))
            {
                await client.SendAsync(request);
                response = await client.ReceiveAsync<DSAlarmShared.Pipe.Response>();
                if (response.Success && response.HasData)
                {
                    cameras = response.Data as List<DSAlarmShared.DS.Camera>;
                    checkedListBoxCameras.Items.AddRange(cameras.ToArray());
                }
            }

            string msg = DSAlarmShared.Log.Logger.Instance.FormatObject("GetCamerasAsync", 
                (DSAlarmShared.DS.System)request.Data, response.Success);
            DSAlarmShared.Log.Logger.Instance.Info(msg);
        }

        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            var rb = sender as RadioButton;
            if (rb != null && rb.Checked)
            {
                panelObserverConfig.Controls.Clear();
                UserControl userControl = null;

                if (rb == radioButtonAdam)
                {
                    _observerType = ObserverType.Adam;
                    userControl = new DSAlarmShared.Observer.Adam.AdamCtrl();

                }
                else if (rb == radioButtonWebService)
                {
                    _observerType = ObserverType.WebService;
                    userControl = new DSAlarmShared.Observer.WebService.WebServiceCtrl();
                }

                if (userControl != null)
                {
                    userControl.Dock = DockStyle.Fill;
                    userControl.AutoSize = true;
                    userControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
                    panelObserverConfig.Controls.Add(userControl);
                }
            }
        }

        private async void ButtonSaveRule_Click(object sender, EventArgs e)
        {
            ShowStatus(true, "Saving rule");
            DSAlarmShared.Observer.IObserver observer = GetObserver();
            SetObserverAlarms(observer);

            var rule = new DSAlarmShared.Rule();
            rule.Cameras = checkedListBoxCameras.CheckedItems.Cast<DSAlarmShared.DS.Camera>().ToList();
            rule.System = new DSAlarmShared.DS.System() { Ip = textBoxIp.Text, Port = ushort.Parse(textBoxPort.Text), Username = textBoxUsername.Text, Password = textBoxPassword.Text };
            rule.Observer = observer;

            if (rule.Cameras.Count == 0 || !rule.Observer.IsComplete)
            {
                ShowStatus(false);
                ShowStatus(true, "Rule not saved, ensure all information is filled in");
                ShowStatus(false);
                return;
            }

            DSAlarmShared.Pipe.Response response;
            var request = new DSAlarmShared.Pipe.Request();
            request.Action = DSAlarmShared.Pipe.Request.PipeAction.AddRule;
            request.Data = rule;

            using (var client = new DSAlarmShared.Pipe.Client(DSAlarmShared.Constants.PipeName))
            {
                await client.SendAsync(request);
                response = await client.ReceiveAsync<DSAlarmShared.Pipe.Response>();
            }
            ShowStatus(false);

            string msg = DSAlarmShared.Log.Logger.Instance.FormatObject("ButtonSaveRule_Click", rule.System, response.Success);
            DSAlarmShared.Log.Logger.Instance.Info(msg);
        }

        private async void ButtonGetRules_Click(object sender, EventArgs e)
        {
            ShowStatus(true, "Getting rules from DSAlarmManager");
            ExistingRulesReset();

            DSAlarmShared.Pipe.Response response;
            var request = new DSAlarmShared.Pipe.Request();
            request.Action = DSAlarmShared.Pipe.Request.PipeAction.GetRules;

            var rules = new List<DSAlarmShared.Rule>();
            using (var client = new DSAlarmShared.Pipe.Client(DSAlarmShared.Constants.PipeName))
            {
                await client.SendAsync(request);
                response = await client.ReceiveAsync<DSAlarmShared.Pipe.Response>();
                if (response.Success && response.HasData)
                    rules = response.Data as List<DSAlarmShared.Rule>;
            }
            checkedListBoxRuleRules.Items.AddRange(rules.ToArray());
            ShowStatus(false);

            string msg = DSAlarmShared.Log.Logger.Instance.FormatObject("ButtonGetRules_Click", response.Success);
            DSAlarmShared.Log.Logger.Instance.Info(msg);
        }

        private void CheckedListBoxRuleRules_SelectedIndexChanged(object sender, EventArgs e)
        {
            var rule = ((CheckedListBox)sender).SelectedItem as DSAlarmShared.Rule;
            if (rule != null)
            {
                checkedListBoxRuleCameras.Items.Clear();
                panelRuleData.Controls.Clear();

                PopulateExistingEvents(rule);
                checkedListBoxRuleCameras.Items.AddRange(rule.Cameras.ToArray());
                for (int i = 0; i < checkedListBoxRuleCameras.Items.Count; i++)
                    checkedListBoxRuleCameras.SetItemChecked(i, true);

                UserControl userControl = null;
                if (rule.Observer is DSAlarmShared.Observer.WebService.WebService)
                {
                    var webService = rule.Observer as DSAlarmShared.Observer.WebService.WebService;
                    userControl = new DSAlarmShared.Observer.WebService.WebServiceCtrl(webService.Data);
                }
                else if(rule.Observer is DSAlarmShared.Observer.Adam.Adam)
                {
                    var adam = rule.Observer as DSAlarmShared.Observer.Adam.Adam;
                    userControl = new DSAlarmShared.Observer.Adam.AdamCtrl(adam.Data);
                }

                if (userControl != null)
                {
                    userControl.Enabled = false;
                    userControl.Dock = DockStyle.Fill;
                    userControl.AutoSize = true;
                    userControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
                    panelRuleData.Controls.Add(userControl);
                }
            }
        }

        private async void ButtonRemoveRules_Click(object sender, EventArgs e)
        {
            int selectedRulesCount = checkedListBoxRuleRules.CheckedIndices.Count;
            if(selectedRulesCount == 0)
                return;

            using (var client = new DSAlarmShared.Pipe.Client(DSAlarmShared.Constants.PipeName))
            {
                var request = new DSAlarmShared.Pipe.Request();
                request.Action = DSAlarmShared.Pipe.Request.PipeAction.RemoveRule;
                request.Option = DSAlarmShared.Pipe.Request.PipeOption.KeepOpen;

                for (int i = 0; i < selectedRulesCount; i++)
                {
                    int ruleIdx = checkedListBoxRuleRules.CheckedIndices[i];
                    var rule = checkedListBoxRuleRules.Items[ruleIdx];
                    request.Data = rule;

                    if (i == selectedRulesCount - 1)
                        request.Option = DSAlarmShared.Pipe.Request.PipeOption.None;

                    await client.SendAsync(request);
                    var response = await client.ReceiveAsync<DSAlarmShared.Pipe.Response>();

                    string msg = DSAlarmShared.Log.Logger.Instance.FormatObject("ButtonRemoveRules_Click", response.Success);
                    DSAlarmShared.Log.Logger.Instance.Info(msg);
                }
            }
            ButtonGetRules_Click(this, EventArgs.Empty);
        }

        private void ExistingRulesReset()
        {
            checkedListBoxRuleRules.Items.Clear();
            tableLayoutPanelRulesEvents.Controls.OfType<CheckBox>().ToList().ForEach(box => box.Checked = false);
            checkedListBoxRuleCameras.Items.Clear();
            panelRuleData.Controls.Clear();
        }

        private DSAlarmShared.Observer.IObserver GetObserver()
        {
            DSAlarmShared.Observer.IObserver observer = null;
            if (_observerType == ObserverType.Adam)
            {
                var adamCtrl = panelObserverConfig.Controls.OfType<DSAlarmShared.Observer.Adam.AdamCtrl>().FirstOrDefault();
                if (adamCtrl != null)
                {
                    var adamData = adamCtrl.Data;
                    observer = new DSAlarmShared.Observer.Adam.Adam(adamData);
                }
            }
            else if (_observerType == ObserverType.WebService)
            {
                var webServiceCtrl = panelObserverConfig.Controls.OfType<DSAlarmShared.Observer.WebService.WebServiceCtrl>().FirstOrDefault();
                if (webServiceCtrl != null)
                {
                    var webServiceData = webServiceCtrl.Data;
                    observer = new DSAlarmShared.Observer.WebService.WebService(webServiceData);
                }
            }
            return observer;
        }

        private async void ShowStatus(bool show = true, string status = "")
        {
            var ts = DateTime.Now - _statusDelayTimer;
            int timeToWaitMs = Math.Max(0, 3000 - (int)ts.TotalMilliseconds);

            if (show)
                _statusDelayTimer = DateTime.Now;
            else
                await Task.Delay(timeToWaitMs);

            toolStripStatusLabel.Text = status;
            toolStripProgressBar.Value = 0;
            toolStripStatusLabel.Visible = show;
            toolStripProgressBar.Visible = show;
        }

        private void PopulateExistingEvents(DSAlarmShared.Rule rule)
        {
            if (rule == null)
                throw new ArgumentNullException("rule");

            bool isChecked = (rule.Observer.EventsObserved & DSAlarmShared.Observer.EventType.Hard) == DSAlarmShared.Observer.EventType.Hard;
            checkBoxRuleHard.Checked = isChecked;
            isChecked = (rule.Observer.EventsObserved & DSAlarmShared.Observer.EventType.Manual) == DSAlarmShared.Observer.EventType.Manual;
            checkBoxRuleManual.Checked = isChecked;
            isChecked = (rule.Observer.EventsObserved & DSAlarmShared.Observer.EventType.Motion) == DSAlarmShared.Observer.EventType.Motion;
            checkBoxRuleMotion.Checked = isChecked;
            isChecked = (rule.Observer.EventsObserved & DSAlarmShared.Observer.EventType.Serial) == DSAlarmShared.Observer.EventType.Serial;
            checkBoxRuleSerial.Checked = isChecked;
            isChecked = (rule.Observer.EventsObserved & DSAlarmShared.Observer.EventType.Soft) == DSAlarmShared.Observer.EventType.Soft;
            checkBoxRuleSoft.Checked = isChecked;
            isChecked = (rule.Observer.EventsObserved & DSAlarmShared.Observer.EventType.Trigger) == DSAlarmShared.Observer.EventType.Trigger;
            checkBoxRuleTrigger.Checked = isChecked;
            isChecked = (rule.Observer.EventsObserved & DSAlarmShared.Observer.EventType.Unknown) == DSAlarmShared.Observer.EventType.Unknown;
            checkBoxRuleUnknown.Checked = isChecked;
            isChecked = (rule.Observer.EventsObserved & DSAlarmShared.Observer.EventType.VideoLoss) == DSAlarmShared.Observer.EventType.VideoLoss;
            checkBoxRuleVideoLoss.Checked = isChecked;
        }

        private void SetObserverAlarms(DSAlarmShared.Observer.IObserver observer)
        {
            if (observer == null)
                throw new ArgumentNullException("observer");

            observer.EventsObserved = 0;
            if (checkBoxHard.Checked)
                observer.EventsObserved |= DSAlarmShared.Observer.EventType.Hard;
            if (checkBoxManual.Checked)
                observer.EventsObserved |= DSAlarmShared.Observer.EventType.Manual;
            if (checkBoxMotion.Checked)
                observer.EventsObserved |= DSAlarmShared.Observer.EventType.Motion;
            if (checkBoxSerial.Checked)
                observer.EventsObserved |= DSAlarmShared.Observer.EventType.Serial;
            if (checkBoxSoft.Checked)
                observer.EventsObserved |= DSAlarmShared.Observer.EventType.Soft;
            if (checkBoxTrigger.Checked)
                observer.EventsObserved |= DSAlarmShared.Observer.EventType.Trigger;
            if (checkBoxUnknown.Checked)
                observer.EventsObserved |= DSAlarmShared.Observer.EventType.Unknown;
            if (checkBoxVideoLoss.Checked)
                observer.EventsObserved |= DSAlarmShared.Observer.EventType.VideoLoss;
        }
    }
}
