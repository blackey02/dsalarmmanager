﻿namespace DSAlarmConfigurator
{
    partial class DSAlarmConfiguratorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DSAlarmConfiguratorForm));
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanelMain2 = new System.Windows.Forms.TableLayoutPanel();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.groupBoxSystem = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxIp = new System.Windows.Forms.TextBox();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.buttonGetCameras = new System.Windows.Forms.Button();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.panelMid = new System.Windows.Forms.Panel();
            this.tableLayoutPanelNewMiddleCol = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.checkBoxUnknown = new System.Windows.Forms.CheckBox();
            this.checkBoxManual = new System.Windows.Forms.CheckBox();
            this.checkBoxTrigger = new System.Windows.Forms.CheckBox();
            this.checkBoxHard = new System.Windows.Forms.CheckBox();
            this.checkBoxVideoLoss = new System.Windows.Forms.CheckBox();
            this.checkBoxMotion = new System.Windows.Forms.CheckBox();
            this.checkBoxSerial = new System.Windows.Forms.CheckBox();
            this.checkBoxSoft = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkedListBoxCameras = new System.Windows.Forms.CheckedListBox();
            this.panelRight = new System.Windows.Forms.Panel();
            this.groupBoxObservers = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelRight = new System.Windows.Forms.TableLayoutPanel();
            this.panelObserverConfig = new System.Windows.Forms.Panel();
            this.buttonSaveRule = new System.Windows.Forms.Button();
            this.flowLayoutPanelObservers = new System.Windows.Forms.FlowLayoutPanel();
            this.radioButtonWebService = new System.Windows.Forms.RadioButton();
            this.radioButtonAdam = new System.Windows.Forms.RadioButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanelRulesRoot = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonGetRules = new System.Windows.Forms.Button();
            this.buttonRemoveRules = new System.Windows.Forms.Button();
            this.tableLayoutPanelRuleMain = new System.Windows.Forms.TableLayoutPanel();
            this.checkedListBoxRuleRules = new System.Windows.Forms.CheckedListBox();
            this.tableLayoutPanelRuleMiddle = new System.Windows.Forms.TableLayoutPanel();
            this.checkedListBoxRuleCameras = new System.Windows.Forms.CheckedListBox();
            this.tableLayoutPanelRulesEvents = new System.Windows.Forms.TableLayoutPanel();
            this.checkBoxRuleUnknown = new System.Windows.Forms.CheckBox();
            this.checkBoxRuleManual = new System.Windows.Forms.CheckBox();
            this.checkBoxRuleTrigger = new System.Windows.Forms.CheckBox();
            this.checkBoxRuleHard = new System.Windows.Forms.CheckBox();
            this.checkBoxRuleVideoLoss = new System.Windows.Forms.CheckBox();
            this.checkBoxRuleMotion = new System.Windows.Forms.CheckBox();
            this.checkBoxRuleSerial = new System.Windows.Forms.CheckBox();
            this.checkBoxRuleSoft = new System.Windows.Forms.CheckBox();
            this.panelRuleData = new System.Windows.Forms.Panel();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControlMain.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanelMain2.SuspendLayout();
            this.panelLeft.SuspendLayout();
            this.groupBoxSystem.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.panelMid.SuspendLayout();
            this.tableLayoutPanelNewMiddleCol.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.groupBoxObservers.SuspendLayout();
            this.tableLayoutPanelRight.SuspendLayout();
            this.flowLayoutPanelObservers.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanelRulesRoot.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanelRuleMain.SuspendLayout();
            this.tableLayoutPanelRuleMiddle.SuspendLayout();
            this.tableLayoutPanelRulesEvents.SuspendLayout();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlMain
            // 
            this.tabControlMain.Controls.Add(this.tabPage1);
            this.tabControlMain.Controls.Add(this.tabPage2);
            this.tabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlMain.Location = new System.Drawing.Point(0, 0);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(681, 279);
            this.tabControlMain.TabIndex = 8;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanelMain2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(673, 253);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "New Rules";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanelMain2
            // 
            this.tableLayoutPanelMain2.ColumnCount = 3;
            this.tableLayoutPanelMain2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanelMain2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanelMain2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelMain2.Controls.Add(this.panelLeft, 0, 0);
            this.tableLayoutPanelMain2.Controls.Add(this.panelMid, 1, 0);
            this.tableLayoutPanelMain2.Controls.Add(this.panelRight, 2, 0);
            this.tableLayoutPanelMain2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelMain2.Name = "tableLayoutPanelMain2";
            this.tableLayoutPanelMain2.RowCount = 1;
            this.tableLayoutPanelMain2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain2.Size = new System.Drawing.Size(667, 247);
            this.tableLayoutPanelMain2.TabIndex = 12;
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.groupBoxSystem);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(3, 3);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(194, 241);
            this.panelLeft.TabIndex = 0;
            // 
            // groupBoxSystem
            // 
            this.groupBoxSystem.Controls.Add(this.tableLayoutPanel7);
            this.groupBoxSystem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxSystem.Location = new System.Drawing.Point(0, 0);
            this.groupBoxSystem.Name = "groupBoxSystem";
            this.groupBoxSystem.Size = new System.Drawing.Size(194, 241);
            this.groupBoxSystem.TabIndex = 7;
            this.groupBoxSystem.TabStop = false;
            this.groupBoxSystem.Text = "Select System";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel7.Controls.Add(this.label2, 0, 3);
            this.tableLayoutPanel7.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.textBoxIp, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.textBoxPort, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.buttonGetCameras, 1, 4);
            this.tableLayoutPanel7.Controls.Add(this.textBoxUsername, 1, 2);
            this.tableLayoutPanel7.Controls.Add(this.textBoxPassword, 1, 3);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 5;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(188, 222);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 85);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 7, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Password";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 59);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 7, 3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Username";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 7, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "IP Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 33);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 7, 3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Port";
            // 
            // textBoxIp
            // 
            this.textBoxIp.Location = new System.Drawing.Point(67, 3);
            this.textBoxIp.Name = "textBoxIp";
            this.textBoxIp.Size = new System.Drawing.Size(85, 20);
            this.textBoxIp.TabIndex = 0;
            this.textBoxIp.Text = "192.168.1.85";
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(67, 29);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(85, 20);
            this.textBoxPort.TabIndex = 1;
            this.textBoxPort.Text = "18772";
            // 
            // buttonGetCameras
            // 
            this.buttonGetCameras.Location = new System.Drawing.Point(67, 107);
            this.buttonGetCameras.Name = "buttonGetCameras";
            this.buttonGetCameras.Size = new System.Drawing.Size(90, 23);
            this.buttonGetCameras.TabIndex = 4;
            this.buttonGetCameras.Text = "Get Cameras";
            this.buttonGetCameras.UseVisualStyleBackColor = true;
            this.buttonGetCameras.Click += new System.EventHandler(this.ButtonGetCameras_Click);
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.Location = new System.Drawing.Point(67, 55);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(85, 20);
            this.textBoxUsername.TabIndex = 2;
            this.textBoxUsername.Text = "admin";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(67, 81);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(85, 20);
            this.textBoxPassword.TabIndex = 3;
            this.textBoxPassword.Text = "admin";
            // 
            // panelMid
            // 
            this.panelMid.Controls.Add(this.tableLayoutPanelNewMiddleCol);
            this.panelMid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMid.Location = new System.Drawing.Point(203, 3);
            this.panelMid.Name = "panelMid";
            this.panelMid.Size = new System.Drawing.Size(194, 241);
            this.panelMid.TabIndex = 1;
            // 
            // tableLayoutPanelNewMiddleCol
            // 
            this.tableLayoutPanelNewMiddleCol.ColumnCount = 1;
            this.tableLayoutPanelNewMiddleCol.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelNewMiddleCol.Controls.Add(this.groupBox3, 0, 0);
            this.tableLayoutPanelNewMiddleCol.Controls.Add(this.groupBox1, 0, 1);
            this.tableLayoutPanelNewMiddleCol.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelNewMiddleCol.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelNewMiddleCol.Name = "tableLayoutPanelNewMiddleCol";
            this.tableLayoutPanelNewMiddleCol.RowCount = 2;
            this.tableLayoutPanelNewMiddleCol.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelNewMiddleCol.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelNewMiddleCol.Size = new System.Drawing.Size(194, 241);
            this.tableLayoutPanelNewMiddleCol.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(188, 118);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Alarm Type";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.checkBoxUnknown, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxManual, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxTrigger, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxHard, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxVideoLoss, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxMotion, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxSerial, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxSoft, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(182, 99);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // checkBoxUnknown
            // 
            this.checkBoxUnknown.AutoSize = true;
            this.checkBoxUnknown.Checked = true;
            this.checkBoxUnknown.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUnknown.Location = new System.Drawing.Point(70, 26);
            this.checkBoxUnknown.Name = "checkBoxUnknown";
            this.checkBoxUnknown.Size = new System.Drawing.Size(72, 17);
            this.checkBoxUnknown.TabIndex = 3;
            this.checkBoxUnknown.Text = "Unknown";
            this.checkBoxUnknown.UseVisualStyleBackColor = true;
            // 
            // checkBoxManual
            // 
            this.checkBoxManual.AutoSize = true;
            this.checkBoxManual.Checked = true;
            this.checkBoxManual.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxManual.Location = new System.Drawing.Point(3, 26);
            this.checkBoxManual.Name = "checkBoxManual";
            this.checkBoxManual.Size = new System.Drawing.Size(61, 17);
            this.checkBoxManual.TabIndex = 2;
            this.checkBoxManual.Text = "Manual";
            this.checkBoxManual.UseVisualStyleBackColor = true;
            // 
            // checkBoxTrigger
            // 
            this.checkBoxTrigger.AutoSize = true;
            this.checkBoxTrigger.Checked = true;
            this.checkBoxTrigger.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxTrigger.Location = new System.Drawing.Point(70, 3);
            this.checkBoxTrigger.Name = "checkBoxTrigger";
            this.checkBoxTrigger.Size = new System.Drawing.Size(59, 17);
            this.checkBoxTrigger.TabIndex = 1;
            this.checkBoxTrigger.Text = "Trigger";
            this.checkBoxTrigger.UseVisualStyleBackColor = true;
            // 
            // checkBoxHard
            // 
            this.checkBoxHard.AutoSize = true;
            this.checkBoxHard.Checked = true;
            this.checkBoxHard.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHard.Location = new System.Drawing.Point(3, 3);
            this.checkBoxHard.Name = "checkBoxHard";
            this.checkBoxHard.Size = new System.Drawing.Size(49, 17);
            this.checkBoxHard.TabIndex = 0;
            this.checkBoxHard.Text = "Hard";
            this.checkBoxHard.UseVisualStyleBackColor = true;
            // 
            // checkBoxVideoLoss
            // 
            this.checkBoxVideoLoss.AutoSize = true;
            this.checkBoxVideoLoss.Checked = true;
            this.checkBoxVideoLoss.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxVideoLoss.Location = new System.Drawing.Point(70, 49);
            this.checkBoxVideoLoss.Name = "checkBoxVideoLoss";
            this.checkBoxVideoLoss.Size = new System.Drawing.Size(75, 17);
            this.checkBoxVideoLoss.TabIndex = 5;
            this.checkBoxVideoLoss.Text = "VideoLoss";
            this.checkBoxVideoLoss.UseVisualStyleBackColor = true;
            // 
            // checkBoxMotion
            // 
            this.checkBoxMotion.AutoSize = true;
            this.checkBoxMotion.Checked = true;
            this.checkBoxMotion.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMotion.Location = new System.Drawing.Point(3, 49);
            this.checkBoxMotion.Name = "checkBoxMotion";
            this.checkBoxMotion.Size = new System.Drawing.Size(58, 17);
            this.checkBoxMotion.TabIndex = 4;
            this.checkBoxMotion.Text = "Motion";
            this.checkBoxMotion.UseVisualStyleBackColor = true;
            // 
            // checkBoxSerial
            // 
            this.checkBoxSerial.AutoSize = true;
            this.checkBoxSerial.Checked = true;
            this.checkBoxSerial.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSerial.Location = new System.Drawing.Point(3, 72);
            this.checkBoxSerial.Name = "checkBoxSerial";
            this.checkBoxSerial.Size = new System.Drawing.Size(52, 17);
            this.checkBoxSerial.TabIndex = 6;
            this.checkBoxSerial.Text = "Serial";
            this.checkBoxSerial.UseVisualStyleBackColor = true;
            // 
            // checkBoxSoft
            // 
            this.checkBoxSoft.AutoSize = true;
            this.checkBoxSoft.Checked = true;
            this.checkBoxSoft.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSoft.Location = new System.Drawing.Point(70, 72);
            this.checkBoxSoft.Name = "checkBoxSoft";
            this.checkBoxSoft.Size = new System.Drawing.Size(45, 17);
            this.checkBoxSoft.TabIndex = 8;
            this.checkBoxSoft.Text = "Soft";
            this.checkBoxSoft.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkedListBoxCameras);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 127);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(188, 111);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cameras";
            // 
            // checkedListBoxCameras
            // 
            this.checkedListBoxCameras.CheckOnClick = true;
            this.checkedListBoxCameras.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxCameras.FormattingEnabled = true;
            this.checkedListBoxCameras.Location = new System.Drawing.Point(3, 16);
            this.checkedListBoxCameras.Name = "checkedListBoxCameras";
            this.checkedListBoxCameras.Size = new System.Drawing.Size(182, 92);
            this.checkedListBoxCameras.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.AutoSize = true;
            this.panelRight.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelRight.Controls.Add(this.groupBoxObservers);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRight.Location = new System.Drawing.Point(403, 3);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(261, 241);
            this.panelRight.TabIndex = 2;
            // 
            // groupBoxObservers
            // 
            this.groupBoxObservers.AutoSize = true;
            this.groupBoxObservers.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBoxObservers.Controls.Add(this.tableLayoutPanelRight);
            this.groupBoxObservers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxObservers.Location = new System.Drawing.Point(0, 0);
            this.groupBoxObservers.Name = "groupBoxObservers";
            this.groupBoxObservers.Size = new System.Drawing.Size(261, 241);
            this.groupBoxObservers.TabIndex = 12;
            this.groupBoxObservers.TabStop = false;
            this.groupBoxObservers.Text = "Endpoint";
            // 
            // tableLayoutPanelRight
            // 
            this.tableLayoutPanelRight.AutoSize = true;
            this.tableLayoutPanelRight.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanelRight.ColumnCount = 1;
            this.tableLayoutPanelRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelRight.Controls.Add(this.panelObserverConfig, 0, 1);
            this.tableLayoutPanelRight.Controls.Add(this.buttonSaveRule, 0, 2);
            this.tableLayoutPanelRight.Controls.Add(this.flowLayoutPanelObservers, 0, 0);
            this.tableLayoutPanelRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelRight.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanelRight.Name = "tableLayoutPanelRight";
            this.tableLayoutPanelRight.RowCount = 3;
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelRight.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelRight.Size = new System.Drawing.Size(255, 222);
            this.tableLayoutPanelRight.TabIndex = 2;
            // 
            // panelObserverConfig
            // 
            this.panelObserverConfig.AutoSize = true;
            this.panelObserverConfig.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelObserverConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelObserverConfig.Location = new System.Drawing.Point(3, 32);
            this.panelObserverConfig.Name = "panelObserverConfig";
            this.panelObserverConfig.Size = new System.Drawing.Size(249, 158);
            this.panelObserverConfig.TabIndex = 1;
            // 
            // buttonSaveRule
            // 
            this.buttonSaveRule.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonSaveRule.Location = new System.Drawing.Point(162, 196);
            this.buttonSaveRule.Name = "buttonSaveRule";
            this.buttonSaveRule.Size = new System.Drawing.Size(90, 23);
            this.buttonSaveRule.TabIndex = 0;
            this.buttonSaveRule.Text = "Save Rule";
            this.buttonSaveRule.UseVisualStyleBackColor = true;
            this.buttonSaveRule.Click += new System.EventHandler(this.ButtonSaveRule_Click);
            // 
            // flowLayoutPanelObservers
            // 
            this.flowLayoutPanelObservers.AutoSize = true;
            this.flowLayoutPanelObservers.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanelObservers.Controls.Add(this.radioButtonWebService);
            this.flowLayoutPanelObservers.Controls.Add(this.radioButtonAdam);
            this.flowLayoutPanelObservers.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanelObservers.Name = "flowLayoutPanelObservers";
            this.flowLayoutPanelObservers.Size = new System.Drawing.Size(151, 23);
            this.flowLayoutPanelObservers.TabIndex = 0;
            // 
            // radioButtonWebService
            // 
            this.radioButtonWebService.AutoSize = true;
            this.radioButtonWebService.Location = new System.Drawing.Point(3, 3);
            this.radioButtonWebService.Name = "radioButtonWebService";
            this.radioButtonWebService.Size = new System.Drawing.Size(87, 17);
            this.radioButtonWebService.TabIndex = 1;
            this.radioButtonWebService.Text = "Web Service";
            this.radioButtonWebService.UseVisualStyleBackColor = true;
            this.radioButtonWebService.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
            // 
            // radioButtonAdam
            // 
            this.radioButtonAdam.AutoSize = true;
            this.radioButtonAdam.Location = new System.Drawing.Point(96, 3);
            this.radioButtonAdam.Name = "radioButtonAdam";
            this.radioButtonAdam.Size = new System.Drawing.Size(52, 17);
            this.radioButtonAdam.TabIndex = 0;
            this.radioButtonAdam.Text = "Adam";
            this.radioButtonAdam.UseVisualStyleBackColor = true;
            this.radioButtonAdam.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanelRulesRoot);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(673, 253);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Existing Rules";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanelRulesRoot
            // 
            this.tableLayoutPanelRulesRoot.ColumnCount = 1;
            this.tableLayoutPanelRulesRoot.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelRulesRoot.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanelRulesRoot.Controls.Add(this.tableLayoutPanelRuleMain, 0, 1);
            this.tableLayoutPanelRulesRoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelRulesRoot.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelRulesRoot.Name = "tableLayoutPanelRulesRoot";
            this.tableLayoutPanelRulesRoot.RowCount = 2;
            this.tableLayoutPanelRulesRoot.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelRulesRoot.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelRulesRoot.Size = new System.Drawing.Size(667, 247);
            this.tableLayoutPanelRulesRoot.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.buttonGetRules);
            this.flowLayoutPanel1.Controls.Add(this.buttonRemoveRules);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(178, 29);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // buttonGetRules
            // 
            this.buttonGetRules.Location = new System.Drawing.Point(3, 3);
            this.buttonGetRules.Name = "buttonGetRules";
            this.buttonGetRules.Size = new System.Drawing.Size(75, 23);
            this.buttonGetRules.TabIndex = 0;
            this.buttonGetRules.Text = "Get Rules";
            this.buttonGetRules.UseVisualStyleBackColor = true;
            this.buttonGetRules.Click += new System.EventHandler(this.ButtonGetRules_Click);
            // 
            // buttonRemoveRules
            // 
            this.buttonRemoveRules.Location = new System.Drawing.Point(84, 3);
            this.buttonRemoveRules.Name = "buttonRemoveRules";
            this.buttonRemoveRules.Size = new System.Drawing.Size(91, 23);
            this.buttonRemoveRules.TabIndex = 1;
            this.buttonRemoveRules.Text = "Remove Rule";
            this.buttonRemoveRules.UseVisualStyleBackColor = true;
            this.buttonRemoveRules.Click += new System.EventHandler(this.ButtonRemoveRules_Click);
            // 
            // tableLayoutPanelRuleMain
            // 
            this.tableLayoutPanelRuleMain.ColumnCount = 3;
            this.tableLayoutPanelRuleMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanelRuleMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanelRuleMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelRuleMain.Controls.Add(this.checkedListBoxRuleRules, 0, 0);
            this.tableLayoutPanelRuleMain.Controls.Add(this.tableLayoutPanelRuleMiddle, 1, 0);
            this.tableLayoutPanelRuleMain.Controls.Add(this.panelRuleData, 2, 0);
            this.tableLayoutPanelRuleMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelRuleMain.Location = new System.Drawing.Point(3, 38);
            this.tableLayoutPanelRuleMain.Name = "tableLayoutPanelRuleMain";
            this.tableLayoutPanelRuleMain.RowCount = 1;
            this.tableLayoutPanelRuleMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelRuleMain.Size = new System.Drawing.Size(661, 206);
            this.tableLayoutPanelRuleMain.TabIndex = 1;
            // 
            // checkedListBoxRuleRules
            // 
            this.checkedListBoxRuleRules.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxRuleRules.FormattingEnabled = true;
            this.checkedListBoxRuleRules.Location = new System.Drawing.Point(3, 3);
            this.checkedListBoxRuleRules.Name = "checkedListBoxRuleRules";
            this.checkedListBoxRuleRules.Size = new System.Drawing.Size(194, 200);
            this.checkedListBoxRuleRules.TabIndex = 0;
            this.checkedListBoxRuleRules.SelectedIndexChanged += new System.EventHandler(this.CheckedListBoxRuleRules_SelectedIndexChanged);
            // 
            // tableLayoutPanelRuleMiddle
            // 
            this.tableLayoutPanelRuleMiddle.ColumnCount = 1;
            this.tableLayoutPanelRuleMiddle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelRuleMiddle.Controls.Add(this.checkedListBoxRuleCameras, 0, 1);
            this.tableLayoutPanelRuleMiddle.Controls.Add(this.tableLayoutPanelRulesEvents, 0, 0);
            this.tableLayoutPanelRuleMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelRuleMiddle.Enabled = false;
            this.tableLayoutPanelRuleMiddle.Location = new System.Drawing.Point(203, 3);
            this.tableLayoutPanelRuleMiddle.Name = "tableLayoutPanelRuleMiddle";
            this.tableLayoutPanelRuleMiddle.RowCount = 2;
            this.tableLayoutPanelRuleMiddle.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelRuleMiddle.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 28.99408F));
            this.tableLayoutPanelRuleMiddle.Size = new System.Drawing.Size(194, 200);
            this.tableLayoutPanelRuleMiddle.TabIndex = 1;
            // 
            // checkedListBoxRuleCameras
            // 
            this.checkedListBoxRuleCameras.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxRuleCameras.FormattingEnabled = true;
            this.checkedListBoxRuleCameras.Location = new System.Drawing.Point(3, 108);
            this.checkedListBoxRuleCameras.Name = "checkedListBoxRuleCameras";
            this.checkedListBoxRuleCameras.Size = new System.Drawing.Size(188, 89);
            this.checkedListBoxRuleCameras.TabIndex = 9;
            // 
            // tableLayoutPanelRulesEvents
            // 
            this.tableLayoutPanelRulesEvents.ColumnCount = 2;
            this.tableLayoutPanelRulesEvents.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelRulesEvents.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelRulesEvents.Controls.Add(this.checkBoxRuleUnknown, 1, 1);
            this.tableLayoutPanelRulesEvents.Controls.Add(this.checkBoxRuleManual, 0, 1);
            this.tableLayoutPanelRulesEvents.Controls.Add(this.checkBoxRuleTrigger, 1, 0);
            this.tableLayoutPanelRulesEvents.Controls.Add(this.checkBoxRuleHard, 0, 0);
            this.tableLayoutPanelRulesEvents.Controls.Add(this.checkBoxRuleVideoLoss, 1, 2);
            this.tableLayoutPanelRulesEvents.Controls.Add(this.checkBoxRuleMotion, 0, 2);
            this.tableLayoutPanelRulesEvents.Controls.Add(this.checkBoxRuleSerial, 0, 3);
            this.tableLayoutPanelRulesEvents.Controls.Add(this.checkBoxRuleSoft, 1, 3);
            this.tableLayoutPanelRulesEvents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelRulesEvents.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelRulesEvents.Name = "tableLayoutPanelRulesEvents";
            this.tableLayoutPanelRulesEvents.RowCount = 4;
            this.tableLayoutPanelRulesEvents.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelRulesEvents.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelRulesEvents.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelRulesEvents.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelRulesEvents.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelRulesEvents.Size = new System.Drawing.Size(188, 99);
            this.tableLayoutPanelRulesEvents.TabIndex = 0;
            // 
            // checkBoxRuleUnknown
            // 
            this.checkBoxRuleUnknown.AutoSize = true;
            this.checkBoxRuleUnknown.Location = new System.Drawing.Point(70, 26);
            this.checkBoxRuleUnknown.Name = "checkBoxRuleUnknown";
            this.checkBoxRuleUnknown.Size = new System.Drawing.Size(72, 17);
            this.checkBoxRuleUnknown.TabIndex = 3;
            this.checkBoxRuleUnknown.Text = "Unknown";
            this.checkBoxRuleUnknown.UseVisualStyleBackColor = true;
            // 
            // checkBoxRuleManual
            // 
            this.checkBoxRuleManual.AutoSize = true;
            this.checkBoxRuleManual.Location = new System.Drawing.Point(3, 26);
            this.checkBoxRuleManual.Name = "checkBoxRuleManual";
            this.checkBoxRuleManual.Size = new System.Drawing.Size(61, 17);
            this.checkBoxRuleManual.TabIndex = 2;
            this.checkBoxRuleManual.Text = "Manual";
            this.checkBoxRuleManual.UseVisualStyleBackColor = true;
            // 
            // checkBoxRuleTrigger
            // 
            this.checkBoxRuleTrigger.AutoSize = true;
            this.checkBoxRuleTrigger.Location = new System.Drawing.Point(70, 3);
            this.checkBoxRuleTrigger.Name = "checkBoxRuleTrigger";
            this.checkBoxRuleTrigger.Size = new System.Drawing.Size(59, 17);
            this.checkBoxRuleTrigger.TabIndex = 1;
            this.checkBoxRuleTrigger.Text = "Trigger";
            this.checkBoxRuleTrigger.UseVisualStyleBackColor = true;
            // 
            // checkBoxRuleHard
            // 
            this.checkBoxRuleHard.AutoSize = true;
            this.checkBoxRuleHard.Location = new System.Drawing.Point(3, 3);
            this.checkBoxRuleHard.Name = "checkBoxRuleHard";
            this.checkBoxRuleHard.Size = new System.Drawing.Size(49, 17);
            this.checkBoxRuleHard.TabIndex = 0;
            this.checkBoxRuleHard.Text = "Hard";
            this.checkBoxRuleHard.UseVisualStyleBackColor = true;
            // 
            // checkBoxRuleVideoLoss
            // 
            this.checkBoxRuleVideoLoss.AutoSize = true;
            this.checkBoxRuleVideoLoss.Location = new System.Drawing.Point(70, 49);
            this.checkBoxRuleVideoLoss.Name = "checkBoxRuleVideoLoss";
            this.checkBoxRuleVideoLoss.Size = new System.Drawing.Size(75, 17);
            this.checkBoxRuleVideoLoss.TabIndex = 5;
            this.checkBoxRuleVideoLoss.Text = "VideoLoss";
            this.checkBoxRuleVideoLoss.UseVisualStyleBackColor = true;
            // 
            // checkBoxRuleMotion
            // 
            this.checkBoxRuleMotion.AutoSize = true;
            this.checkBoxRuleMotion.Location = new System.Drawing.Point(3, 49);
            this.checkBoxRuleMotion.Name = "checkBoxRuleMotion";
            this.checkBoxRuleMotion.Size = new System.Drawing.Size(58, 17);
            this.checkBoxRuleMotion.TabIndex = 4;
            this.checkBoxRuleMotion.Text = "Motion";
            this.checkBoxRuleMotion.UseVisualStyleBackColor = true;
            // 
            // checkBoxRuleSerial
            // 
            this.checkBoxRuleSerial.AutoSize = true;
            this.checkBoxRuleSerial.Location = new System.Drawing.Point(3, 72);
            this.checkBoxRuleSerial.Name = "checkBoxRuleSerial";
            this.checkBoxRuleSerial.Size = new System.Drawing.Size(52, 17);
            this.checkBoxRuleSerial.TabIndex = 6;
            this.checkBoxRuleSerial.Text = "Serial";
            this.checkBoxRuleSerial.UseVisualStyleBackColor = true;
            // 
            // checkBoxRuleSoft
            // 
            this.checkBoxRuleSoft.AutoSize = true;
            this.checkBoxRuleSoft.Location = new System.Drawing.Point(70, 72);
            this.checkBoxRuleSoft.Name = "checkBoxRuleSoft";
            this.checkBoxRuleSoft.Size = new System.Drawing.Size(45, 17);
            this.checkBoxRuleSoft.TabIndex = 8;
            this.checkBoxRuleSoft.Text = "Soft";
            this.checkBoxRuleSoft.UseVisualStyleBackColor = true;
            // 
            // panelRuleData
            // 
            this.panelRuleData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRuleData.Location = new System.Drawing.Point(403, 3);
            this.panelRuleData.Name = "panelRuleData";
            this.panelRuleData.Size = new System.Drawing.Size(255, 200);
            this.panelRuleData.TabIndex = 2;
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.AutoScroll = true;
            this.toolStripContainer1.ContentPanel.Controls.Add(this.tabControlMain);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(681, 279);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.LeftToolStripPanelVisible = false;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.RightToolStripPanelVisible = false;
            this.toolStripContainer1.Size = new System.Drawing.Size(681, 301);
            this.toolStripContainer1.TabIndex = 13;
            this.toolStripContainer1.Text = "toolStripContainer1";
            this.toolStripContainer1.TopToolStripPanelVisible = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar,
            this.toolStripStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(681, 22);
            this.statusStrip1.TabIndex = 0;
            // 
            // toolStripProgressBar
            // 
            this.toolStripProgressBar.Name = "toolStripProgressBar";
            this.toolStripProgressBar.Size = new System.Drawing.Size(100, 16);
            this.toolStripProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel.Text = "toolStripStatusLabel1";
            // 
            // DSAlarmConfiguratorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 301);
            this.Controls.Add(this.toolStripContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(680, 306);
            this.Name = "DSAlarmConfiguratorForm";
            this.Text = "DS Alarm Configurator";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DSAlarmConfiguratorForm_FormClosing);
            this.tabControlMain.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanelMain2.ResumeLayout(false);
            this.tableLayoutPanelMain2.PerformLayout();
            this.panelLeft.ResumeLayout(false);
            this.groupBoxSystem.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.panelMid.ResumeLayout(false);
            this.tableLayoutPanelNewMiddleCol.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.groupBoxObservers.ResumeLayout(false);
            this.groupBoxObservers.PerformLayout();
            this.tableLayoutPanelRight.ResumeLayout(false);
            this.tableLayoutPanelRight.PerformLayout();
            this.flowLayoutPanelObservers.ResumeLayout(false);
            this.flowLayoutPanelObservers.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanelRulesRoot.ResumeLayout(false);
            this.tableLayoutPanelRulesRoot.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanelRuleMain.ResumeLayout(false);
            this.tableLayoutPanelRuleMiddle.ResumeLayout(false);
            this.tableLayoutPanelRulesEvents.ResumeLayout(false);
            this.tableLayoutPanelRulesEvents.PerformLayout();
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelRulesRoot;
        private System.Windows.Forms.Button buttonGetRules;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelRuleMain;
        private System.Windows.Forms.CheckedListBox checkedListBoxRuleRules;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelRuleMiddle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelRulesEvents;
        private System.Windows.Forms.CheckBox checkBoxRuleUnknown;
        private System.Windows.Forms.CheckBox checkBoxRuleManual;
        private System.Windows.Forms.CheckBox checkBoxRuleTrigger;
        private System.Windows.Forms.CheckBox checkBoxRuleHard;
        private System.Windows.Forms.CheckBox checkBoxRuleVideoLoss;
        private System.Windows.Forms.CheckBox checkBoxRuleMotion;
        private System.Windows.Forms.CheckBox checkBoxRuleSerial;
        private System.Windows.Forms.CheckBox checkBoxRuleSoft;
        private System.Windows.Forms.Panel panelRuleData;
        private System.Windows.Forms.GroupBox groupBoxObservers;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelRight;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelObservers;
        private System.Windows.Forms.RadioButton radioButtonWebService;
        private System.Windows.Forms.RadioButton radioButtonAdam;
        private System.Windows.Forms.Panel panelObserverConfig;
        private System.Windows.Forms.Button buttonSaveRule;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelNewMiddleCol;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.CheckBox checkBoxUnknown;
        private System.Windows.Forms.CheckBox checkBoxManual;
        private System.Windows.Forms.CheckBox checkBoxTrigger;
        private System.Windows.Forms.CheckBox checkBoxHard;
        private System.Windows.Forms.CheckBox checkBoxVideoLoss;
        private System.Windows.Forms.CheckBox checkBoxMotion;
        private System.Windows.Forms.CheckBox checkBoxSerial;
        private System.Windows.Forms.CheckBox checkBoxSoft;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckedListBox checkedListBoxCameras;
        private System.Windows.Forms.GroupBox groupBoxSystem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxIp;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.Button buttonGetCameras;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain2;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelMid;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button buttonRemoveRules;
        private System.Windows.Forms.CheckedListBox checkedListBoxRuleCameras;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
    }
}

