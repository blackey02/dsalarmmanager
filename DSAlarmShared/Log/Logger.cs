﻿using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSAlarmShared.Log
{
    public class Logger
    {
        private static Logger _instance = null;
        private static readonly object _instanceLock = new object();

        public static Logger Instance
        {
            get
            {
                lock (_instanceLock)
                {
                    if (_instance == null)
                        _instance = new Logger();
                    return _instance;
                }
            }
        }

        private ILog _logger = LogManager.GetLogger(typeof(Logger));
        public string ComponentId { get; set; }

        private Logger()
        {
            Setup();
            ComponentId = "Global";
        }

        private void Setup()
        {
            Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();

            PatternLayout patternLayout = new PatternLayout();
            patternLayout.ConversionPattern = "%date [%thread] %-5level - %message%newline";
            patternLayout.ActivateOptions();

            RollingFileAppender roller = new RollingFileAppender();
            roller.AppendToFile = true;
            roller.File = string.Format("Logs\\{0}Log.txt", System.Reflection.Assembly.GetEntryAssembly().GetName().Name);
            roller.Layout = patternLayout;
            roller.MaxSizeRollBackups = 5;
            roller.MaximumFileSize = "1GB";
            roller.RollingStyle = RollingFileAppender.RollingMode.Size;
            roller.StaticLogFileName = true;
            roller.ActivateOptions();
            hierarchy.Root.AddAppender(roller);

            hierarchy.Root.Level = Level.Info;
            hierarchy.Configured = true;
        }

        private object PrependIdIfString(object message)
        {
            object retMessage = message;
            if (retMessage is string)
                retMessage = string.Format("{0}::{1}", ComponentId, retMessage);
            return retMessage;
        }

        public void Debug(object message)
        {
            _logger.Debug(PrependIdIfString(message));
        }

        public void Debug(object message, Exception exception)
        {
            _logger.Debug(PrependIdIfString(message), exception);
        }

        public void Info(object message)
        {
            _logger.Info(PrependIdIfString(message));
        }

        public void Info(object message, Exception exception)
        {
            _logger.Info(PrependIdIfString(message), exception);
        }

        public void Warn(object message)
        {
            _logger.Warn(PrependIdIfString(message));
        }

        public void Warn(object message, Exception exception)
        {
            _logger.Warn(PrependIdIfString(message), exception);
        }

        public void Fatal(object message)
        {
            _logger.Fatal(PrependIdIfString(message));
        }

        public void Fatal(object message, Exception exception)
        {
            _logger.Fatal(PrependIdIfString(message), exception);
        }

        public string FormatObject(string id, DSAlarmShared.DS.System system, bool wasSuccess, string additional = null)
        {
            var format = "{0}: {1}: User: {2}: From: {3} - {4}";
            additional = additional ?? string.Empty;
            var msg = string.Format(format, id, wasSuccess ? "Success" : "Failed", system.Username, system.Ip, additional);
            return msg;
        }

        public string FormatObject(string id, bool wasSuccess, string additional = null)
        {
            var format = "{0}: {1} - {2}";
            additional = additional ?? string.Empty;
            var msg = string.Format(format, id, wasSuccess ? "Success" : "Failed", additional);
            return msg;
        }
    }
}
