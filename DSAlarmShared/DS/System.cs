﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSAlarmShared.DS
{
    [Serializable]
    public class System
    {
        public System()
        { }

        public System(System system)
        {
            Ip = system.Ip;
            Port = system.Port;
            Name = system.Name;
            Username = system.Username;
            Password = system.Password;
        }

        public string Ip { get; set; }
        public ushort Port { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
