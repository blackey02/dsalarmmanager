﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSAlarmShared.DS
{
    [Serializable]
    public class Camera
    {
        public Camera()
        { }

        public Camera(Camera camera)
        {
            FriendlyName = camera.FriendlyName;
            CameraID = camera.CameraID;
            Detected = camera.Detected;
            IsIpCamera = camera.IsIpCamera;
            IsPtz = camera.IsPtz;
            Number = camera.Number;
        }

        public string FriendlyName { get; set; }
        public int CameraID { get; set; }
        public bool Detected { get; set; }
        public bool IsIpCamera { get; set; }
        public bool IsPtz { get; set; }
        public int Number { get; set; }

        public override string ToString()
        {
            return FriendlyName;
        }
    }
}
