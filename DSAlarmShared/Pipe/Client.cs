﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace DSAlarmShared.Pipe
{
    public class Client : IDisposable
    {
        private BinaryFormatter _formatter;
        private NamedPipeClientStream _pipe;

        public Client(string pipeName)
        {
            _pipe = new NamedPipeClientStream(".", pipeName, PipeDirection.InOut);
            _pipe.Connect();
            _formatter = new BinaryFormatter();
        }

        public async Task SendAsync(Request request)
        {
            await Task.Run(() => _formatter.Serialize(_pipe, request));
        }

        public async Task<T> ReceiveAsync<T>()
        {
            T response = default(T);
            await Task.Run(() => response = (T)_formatter.Deserialize(_pipe));
            return response;
        }

        public void Dispose()
        {
            _pipe.Dispose();
        }
    }
}
