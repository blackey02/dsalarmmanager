﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSAlarmShared.Pipe
{
    [Serializable]
    public class Request
    {
        public enum PipeAction
        {
            AddRule,
            GetRules,
            RemoveRule,
            GetCameras
        }

        [Flags]
        public enum PipeOption
        { 
            None = 0x0,
            KeepOpen = 0x1
        }

        public PipeAction Action { get; set; }
        public PipeOption Option { get; set; }
        public object Data { get; set; }
    }
}
