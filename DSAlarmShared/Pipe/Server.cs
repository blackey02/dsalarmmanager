﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DSAlarmShared.Pipe
{
    public class Server : IDisposable
    {
        public event Action<int> ClientConnected;
        private BinaryFormatter _formatter;
        private List<NamedPipeServerStream> _pipes = new List<NamedPipeServerStream>(100);

        public Server(string pipeName)
        {
            for (int i = 0; i < _pipes.Capacity; i++)
                _pipes.Add(new NamedPipeServerStream(pipeName, PipeDirection.InOut, _pipes.Capacity));
            _formatter = new BinaryFormatter();
        }

        public void DisconnectClient(int id)
        {
            var pipe = _pipes[id];
            pipe.Disconnect();
        }

        private void FireClientConnected(int id)
        {
            if (ClientConnected != null)
                ClientConnected(id);
        }

        public void WaitForClientsAsync()
        {
            for(int i = 0; i < _pipes.Capacity; i++)
            {
                var pipe = _pipes[i];
                int id = i;
                Task.Run(() => 
                {
                    while(true)
                    {
                        pipe.WaitForConnection();
                        FireClientConnected(id);
                    }
                });
            }
        }

        public void Send(int id, object request)
        {
            var pipe = _pipes[id];
            _formatter.Serialize(pipe, request);
        }

        public T Receive<T>(int id)
        {
            var pipe = _pipes[id];
            return (T)_formatter.Deserialize(pipe);
        }

        public void Dispose()
        {
            foreach(var pipe in _pipes)
                pipe.Dispose();
        }
    }
}
