﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSAlarmShared.Pipe
{
    [Serializable]
    public class Response
    {
        public bool Success { get; set; }
        public object Data { get; set; }
        public bool HasData { get { return Data != null; } }
    }
}
