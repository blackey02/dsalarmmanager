﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSAlarmShared
{
    [Serializable]
    public class Rule
    {
        private string _id = Guid.NewGuid().ToString();

        public Rule()
        { }

        public Rule(Rule rule)
        {
            _id = rule.Id;
            System = new DS.System(rule.System);
            Cameras = rule.Cameras.ConvertAll(cam => new DSAlarmShared.DS.Camera(cam));
            this.Observer = (Observer.IObserver)Activator.CreateInstance(rule.Observer.GetType(), rule.Observer);
        }

        public string Id { get { return _id; } }
        public DS.System System { get; set; }
        public List<DS.Camera> Cameras { get; set; }
        public Observer.IObserver Observer { get; set; }
    }
}
