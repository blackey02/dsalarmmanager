﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSAlarmShared.Observer.WebService
{
    [Serializable]
    public class WebServiceCtrlData
    {
        public Uri Url { get; set; }
        public string Verb { get; set; }
        public string ContentType { get; set; }
        public string HeaderName { get; set; }
        public string HeaderValue { get; set; }
        public string HeaderName2 { get; set; }
        public string HeaderValue2 { get; set; }
        public string HeaderName3 { get; set; }
        public string HeaderValue3 { get; set; }
        public string Body { get; set; }
        public string BaseUrl
        {
            get
            {
                return string.Format("{0}://{1}", Url.Scheme, Url.Authority);
            }
        }
        public string Resource
        {
            get
            {
                return Url.PathAndQuery;
            }
        }        
    }
}