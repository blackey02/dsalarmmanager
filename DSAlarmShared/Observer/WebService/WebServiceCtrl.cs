﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DSAlarmShared.Observer.WebService
{
    public partial class WebServiceCtrl : UserControl
    {
        public WebServiceCtrl()
        {
            InitializeComponent();
        }

        public WebServiceCtrl(WebServiceCtrlData data)
            : this()
        {
            Data = data;
        }

        public WebServiceCtrlData Data
        {
            get
            {
                var data = new WebServiceCtrlData();
                data.Url = new Uri(textBoxUrl.Text);
                data.Verb = textBoxVerb.Text;
                data.ContentType = textBoxContentType.Text;
                data.HeaderName = textBoxHeaderName.Text;
                data.HeaderValue = textBoxHeaderValue.Text;
                data.HeaderName2 = textBoxHeaderName2.Text;
                data.HeaderValue2 = textBoxHeaderValue2.Text;
                data.HeaderName3 = textBoxHeaderName3.Text;
                data.HeaderValue3 = textBoxHeaderValue3.Text;
                data.Body = textBoxData.Text;
                return data;
            }
            set
            {
                textBoxUrl.Text = value.Url.ToString();
                textBoxVerb.Text = value.Verb;
                textBoxContentType.Text = value.ContentType;
                textBoxHeaderName.Text = value.HeaderName;
                textBoxHeaderValue.Text = value.HeaderValue;
                textBoxHeaderName2.Text = value.HeaderName2;
                textBoxHeaderValue2.Text = value.HeaderValue2;
                textBoxHeaderName3.Text = value.HeaderName3;
                textBoxHeaderValue3.Text = value.HeaderValue3;
                textBoxData.Text = value.Body;
            }
        }
    }
}
