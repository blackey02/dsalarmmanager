﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSAlarmShared.Observer.WebService
{
    [Serializable]
    public class WebService : BaseObserver
    {
        public WebServiceCtrlData Data { get; private set; }

        public WebService(WebServiceCtrlData data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            Data = data;
        }

        public WebService(WebService service)
            : base(service)
        {
            Data = service.Data;
        }

        public override bool IsComplete
        {
            get
            {
                bool isComplete = true;
                isComplete &= base.IsComplete;
                isComplete &= Data.Url != null;
                isComplete &= !string.IsNullOrWhiteSpace(Data.Url.Host);
                isComplete &= !string.IsNullOrWhiteSpace(Data.Body);
                isComplete &= !string.IsNullOrWhiteSpace(Data.Verb);
                isComplete &= !string.IsNullOrWhiteSpace(Data.ContentType);
                return isComplete;
            }
        }

        public override void OnEvent(Notification notification)
        {
            SendAsync(notification);
        }

        private void SendAsync(Notification notification)
        {
            string responseStr = string.Empty;

            try
            {
                var verbMethod = (RestSharp.Method)Enum.Parse(typeof(RestSharp.Method), Data.Verb, true);

                var client = new RestSharp.RestClient(Data.BaseUrl);
                var request = new RestSharp.RestRequest(Data.Resource, verbMethod);

                if (!string.IsNullOrWhiteSpace(Data.HeaderName))
                    request.AddHeader(Data.HeaderName, Data.HeaderValue);
                if (!string.IsNullOrWhiteSpace(Data.HeaderName2))
                    request.AddHeader(Data.HeaderName2, Data.HeaderValue2);
                if (!string.IsNullOrWhiteSpace(Data.HeaderName3))
                    request.AddHeader(Data.HeaderName3, Data.HeaderValue3);

                request.AddParameter(Data.ContentType, MakeReplacements(notification), RestSharp.ParameterType.RequestBody);

                Task.Run(() => client.Execute(request));
            }
            catch (Exception e)
            {
                string msg = DSAlarmShared.Log.Logger.Instance.FormatObject("SendAsync", false);
                DSAlarmShared.Log.Logger.Instance.Warn(msg, e);
            }
        }

        private string MakeReplacements(Notification notification)
        {
            string replacedBody = Data.Body;
            replacedBody = replacedBody.Replace("{{ALARM_DESCRIPTION}}", notification.AlarmDescription);
            replacedBody = replacedBody.Replace("{{CAMERA_ID}}", notification.Camera.CameraID.ToString());
            replacedBody = replacedBody.Replace("{{CAMERA_DETECTED}}", notification.Camera.Detected ? "true" : "false");
            replacedBody = replacedBody.Replace("{{CAMERA_NAME}}", notification.Camera.FriendlyName);
            replacedBody = replacedBody.Replace("{{CAMERA_IS_IP}}", notification.Camera.IsIpCamera ? "true" : "false");
            replacedBody = replacedBody.Replace("{{CAMERA_IS_PTZ}}", notification.Camera.IsPtz ? "true" : "false");
            replacedBody = replacedBody.Replace("{{CAMERA_NUMBER}}", notification.Camera.Number.ToString());
            replacedBody = replacedBody.Replace("{{IS_ALARMED}}", notification.IsAlarmed ? "true" : "false");
            replacedBody = replacedBody.Replace("{{ALARM_TYPE}}", Enum.GetName(typeof(EventType), notification.RecorderEventType));
            replacedBody = replacedBody.Replace("{{SYSTEM_IP}}", notification.System.Ip);
            replacedBody = replacedBody.Replace("{{SYSTEM_PORT}}", notification.System.Port.ToString());
            replacedBody = replacedBody.Replace("{{SYSTEM_NAME}}", notification.System.Name);
            replacedBody = replacedBody.Replace("{{TIMESTAMP}}", notification.Timestamp.ToString());
            return replacedBody;
        }
    }
}
