﻿namespace DSAlarmShared.Observer.WebService
{
    partial class WebServiceCtrl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WebServiceCtrl));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxVerb = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxUrl = new System.Windows.Forms.TextBox();
            this.textBoxContentType = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxHeaderName = new System.Windows.Forms.TextBox();
            this.textBoxHeaderValue = new System.Windows.Forms.TextBox();
            this.textBoxHeaderName3 = new System.Windows.Forms.TextBox();
            this.textBoxHeaderValue3 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxData = new System.Windows.Forms.TextBox();
            this.textBoxHeaderName2 = new System.Windows.Forms.TextBox();
            this.textBoxHeaderValue2 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.textBoxVerb, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxUrl, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBoxContentType, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.textBoxHeaderName, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBoxHeaderValue, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBoxHeaderName3, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.textBoxHeaderValue3, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.textBoxData, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.textBoxHeaderName2, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.textBoxHeaderValue2, 1, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(360, 484);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // textBoxVerb
            // 
            this.textBoxVerb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxVerb.Location = new System.Drawing.Point(183, 48);
            this.textBoxVerb.Name = "textBoxVerb";
            this.textBoxVerb.Size = new System.Drawing.Size(174, 20);
            this.textBoxVerb.TabIndex = 2;
            this.textBoxVerb.Text = "POST";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Margin = new System.Windows.Forms.Padding(3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "URL";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 52);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 7, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Verb";
            // 
            // textBoxUrl
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.textBoxUrl, 2);
            this.textBoxUrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxUrl.Location = new System.Drawing.Point(3, 22);
            this.textBoxUrl.Name = "textBoxUrl";
            this.textBoxUrl.Size = new System.Drawing.Size(354, 20);
            this.textBoxUrl.TabIndex = 1;
            this.textBoxUrl.Text = "http://mydomain.com/api";
            // 
            // textBoxContentType
            // 
            this.textBoxContentType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxContentType.Location = new System.Drawing.Point(183, 74);
            this.textBoxContentType.Name = "textBoxContentType";
            this.textBoxContentType.Size = new System.Drawing.Size(174, 20);
            this.textBoxContentType.TabIndex = 3;
            this.textBoxContentType.Text = "application/json";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 78);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 7, 3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Content-Type";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 100);
            this.label4.Margin = new System.Windows.Forms.Padding(3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "[Optional Headers]";
            // 
            // textBoxHeaderName
            // 
            this.textBoxHeaderName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxHeaderName.Location = new System.Drawing.Point(3, 119);
            this.textBoxHeaderName.Name = "textBoxHeaderName";
            this.textBoxHeaderName.Size = new System.Drawing.Size(174, 20);
            this.textBoxHeaderName.TabIndex = 4;
            // 
            // textBoxHeaderValue
            // 
            this.textBoxHeaderValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxHeaderValue.Location = new System.Drawing.Point(183, 119);
            this.textBoxHeaderValue.Name = "textBoxHeaderValue";
            this.textBoxHeaderValue.Size = new System.Drawing.Size(174, 20);
            this.textBoxHeaderValue.TabIndex = 5;
            // 
            // textBoxHeaderName3
            // 
            this.textBoxHeaderName3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxHeaderName3.Location = new System.Drawing.Point(3, 171);
            this.textBoxHeaderName3.Name = "textBoxHeaderName3";
            this.textBoxHeaderName3.Size = new System.Drawing.Size(174, 20);
            this.textBoxHeaderName3.TabIndex = 8;
            // 
            // textBoxHeaderValue3
            // 
            this.textBoxHeaderValue3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxHeaderValue3.Location = new System.Drawing.Point(183, 171);
            this.textBoxHeaderValue3.Name = "textBoxHeaderValue3";
            this.textBoxHeaderValue3.Size = new System.Drawing.Size(174, 20);
            this.textBoxHeaderValue3.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 197);
            this.label5.Margin = new System.Windows.Forms.Padding(3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "[Template]";
            // 
            // textBoxData
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.textBoxData, 2);
            this.textBoxData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxData.Location = new System.Drawing.Point(3, 216);
            this.textBoxData.Multiline = true;
            this.textBoxData.Name = "textBoxData";
            this.textBoxData.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxData.Size = new System.Drawing.Size(354, 265);
            this.textBoxData.TabIndex = 10;
            this.textBoxData.Text = resources.GetString("textBoxData.Text");
            // 
            // textBoxHeaderName2
            // 
            this.textBoxHeaderName2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxHeaderName2.Location = new System.Drawing.Point(3, 145);
            this.textBoxHeaderName2.Name = "textBoxHeaderName2";
            this.textBoxHeaderName2.Size = new System.Drawing.Size(174, 20);
            this.textBoxHeaderName2.TabIndex = 6;
            // 
            // textBoxHeaderValue2
            // 
            this.textBoxHeaderValue2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxHeaderValue2.Location = new System.Drawing.Point(183, 145);
            this.textBoxHeaderValue2.Name = "textBoxHeaderValue2";
            this.textBoxHeaderValue2.Size = new System.Drawing.Size(174, 20);
            this.textBoxHeaderValue2.TabIndex = 7;
            // 
            // WebServiceCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "WebServiceCtrl";
            this.Size = new System.Drawing.Size(360, 484);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox textBoxVerb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxUrl;
        private System.Windows.Forms.TextBox textBoxContentType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxHeaderName;
        private System.Windows.Forms.TextBox textBoxHeaderValue;
        private System.Windows.Forms.TextBox textBoxHeaderName3;
        private System.Windows.Forms.TextBox textBoxHeaderValue3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxData;
        private System.Windows.Forms.TextBox textBoxHeaderName2;
        private System.Windows.Forms.TextBox textBoxHeaderValue2;
    }
}
