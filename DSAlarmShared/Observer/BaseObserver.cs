﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSAlarmShared.Observer
{
    [Serializable]
    public abstract class BaseObserver : IObserver
    {
        private EventType _eventType;

        public BaseObserver()
        { }

        public BaseObserver(BaseObserver observer)
        {
            EventsObserved = observer.EventsObserved;
        }

        public virtual EventType EventsObserved
        {
            get
            {
                return _eventType;
            }
            set
            {
                _eventType = value;
            }
        }

        public virtual bool IsComplete 
        {
            get
            {
                bool isComplete = true;
                isComplete &= EventsObserved != EventType.NotSpecified;
                return isComplete;
            }
        }

        public abstract void OnEvent(Notification notification);
    }
}
