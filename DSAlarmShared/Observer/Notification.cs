﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSAlarmShared.Observer
{
    public class Notification
    {
        public DS.System System { get; set; }
        public DS.Camera Camera { get; set; }
        public EventType RecorderEventType { get; set; }
        public string AlarmDescription { get; set; }
        public bool IsAlarmed { get; set; }
        public DateTime Timestamp { get; set; }
        public string AlarmId
        {
            get
            {
                return string.Format("{0}:{1}:{2}", Camera.CameraID, Camera.FriendlyName,
                    Enum.GetName(RecorderEventType.GetType(), RecorderEventType));
            }
        }
    }
}
