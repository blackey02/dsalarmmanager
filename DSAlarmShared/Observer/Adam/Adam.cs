﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace DSAlarmShared.Observer.Adam
{
    [Serializable]
    public class Adam : BaseObserver
    {
        public AdamCtrlData Data { get; private set; }
        [NonSerialized]
        private Advantech.Adam.AdamSocket _adamSocket;

        public Adam(AdamCtrlData data)
            : base()
        {
            if (data == null)
                throw new ArgumentNullException("data");
            Data = data;
            Common();
        }

        public Adam(Adam adam)
            : base(adam)
        {
            Data = adam.Data;
            Common();
        }

        private void Common()
        {
            _adamSocket = new Advantech.Adam.AdamSocket();
            _adamSocket.SetTimeout(1000, 1000, 1000);
        }

        public override bool IsComplete
        {
            get 
            {
                bool isComplete = true;
                isComplete &= base.IsComplete;
                isComplete &= Data.Url != null;
                isComplete &= !string.IsNullOrWhiteSpace(Data.Url.Host);
                isComplete &= Data.CheckData.Count > 0;
                return isComplete;
            }
        }

        public override void OnEvent(Notification notification)
        {
            NotifyAdamAsync(notification);
        }

        private void NotifyAdamAsync(Notification notification)
        {
            string responseStr = string.Empty;

            try
            {
                if (_adamSocket.Connect(Data.Url.Host, ProtocolType.Tcp, 502))
                {
                    int doStart = 17;
                    bool enableRelay = notification.IsAlarmed;

                    foreach (var checkedItem in Data.CheckData.Where(item => item.IsChecked))
                        _adamSocket.Modbus().ForceSingleCoil(doStart + checkedItem.ChannelIdx, enableRelay);

                    _adamSocket.Disconnect();
                }
            }
            catch (Exception e)
            {
                string msg = DSAlarmShared.Log.Logger.Instance.FormatObject("NotifyAdamAsync", false);
                DSAlarmShared.Log.Logger.Instance.Warn(msg, e);
            }
        }
    }
}
