﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSAlarmShared.Observer.Adam
{
    [Serializable]
    public class AdamCtrlData
    {
        [Serializable]
        public class AdamCheckData
        {
            public int ChannelIdx { get; set; }
            public bool IsChecked { get; set; }

            public override string ToString()
            {
                return string.Format("Channel {0}", ChannelIdx + 1);
            }
        }

        /// ////////////////////////////////////////////////////////////////////////////////

        public Uri Url { get; set; }
        public List<AdamCheckData> CheckData { get; set; }

        public string BaseUrl
        {
            get
            {
                return string.Format("{0}://{1}", Url.Scheme, Url.Authority);
            }
        }
        public string Resource
        {
            get
            {
                return Url.PathAndQuery;
            }
        }
    }
}
