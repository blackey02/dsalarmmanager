﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Net.Sockets;

namespace DSAlarmShared.Observer.Adam
{
    public partial class AdamCtrl : UserControl
    {
        private Advantech.Adam.AdamSocket _adamSocket;

        public AdamCtrl()
        {
            InitializeComponent();
            _adamSocket = new Advantech.Adam.AdamSocket();
            _adamSocket.SetTimeout(1000, 1000, 1000);
        }

        public AdamCtrl(AdamCtrlData data)
            : this()
        {
            Data = data;
        }

        private void ButtonDetectChannels_Click(object sender, EventArgs e)
        {
            checkedListBoxChannels.Items.Clear();
            string responseStr = string.Empty;

            try
            {
                bool success = false;
                bool[] channels = null;

                if(_adamSocket.Connect(Data.Url.Host, ProtocolType.Tcp, 502))
                {
                    int totalChannels = 6;
                    int doStart = 17;

                    success = _adamSocket.Modbus().ReadCoilStatus(doStart, totalChannels, out channels);
                    _adamSocket.Disconnect();
                }

                if (success)
                {
                    for (int i = 0; i < channels.Length; i++)
                    {
                        bool isChecked = channels[i];
                        checkedListBoxChannels.Items.Add(new AdamCtrlData.AdamCheckData() { ChannelIdx = i, IsChecked = isChecked });
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = DSAlarmShared.Log.Logger.Instance.FormatObject("ButtonDetectChannels_Click", false);
                DSAlarmShared.Log.Logger.Instance.Warn(msg, ex);
            }
        }

        public AdamCtrlData Data
        {
            get
            {
                var data = new AdamCtrlData();
                var ip = string.IsNullOrWhiteSpace(textBoxIp.Text) ? "127.0.0.1" : textBoxIp.Text;
                data.Url = new Uri(string.Format("http://{0}/digitaloutput/all/value", ip));
                data.CheckData = new List<AdamCtrlData.AdamCheckData>();
                for(int i = 0; i < checkedListBoxChannels.Items.Count; i++)
                {
                    AdamCtrlData.AdamCheckData checkedItem = (AdamCtrlData.AdamCheckData)checkedListBoxChannels.Items[i];
                    bool isChecked = checkedListBoxChannels.GetItemChecked(i);
                    data.CheckData.Add(new AdamCtrlData.AdamCheckData() { ChannelIdx = checkedItem.ChannelIdx, IsChecked = isChecked });
                }
                return data;
            }
            set
            {
                textBoxIp.Text = value.Url.Host;
                foreach (AdamCtrlData.AdamCheckData checkData in value.CheckData)
                    checkedListBoxChannels.Items.Add(checkData, checkData.IsChecked);
            }
        }
    }
}
