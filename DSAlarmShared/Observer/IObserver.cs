﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSAlarmShared.Observer
{
    [Flags]
    public enum EventType
    {
        NotSpecified = 0x0,
        Hard = 0x1,
        Manual = 0x2,
        Motion = 0x4,
        Serial = 0x8,
        Soft = 0x10,
        Trigger = 0x20,
        Unknown = 0x40,
        VideoLoss = 0x80,
        All = Hard | Manual | Motion | Serial | Soft | Trigger | Unknown | VideoLoss
    }

    public interface IObserver
    {
        EventType EventsObserved { get; set; }
        bool IsComplete { get; }
        void OnEvent(Notification notification);
    }
}
