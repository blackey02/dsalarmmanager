DSAlarmManager
====================
DSAlarmManager contains an application (DSAlarmConfigurator) and a service (DSAlarmManager) which together allow you to send out events based on certain rules for a particular Pelco Digital Sentry system. A rule is basically an encapsulation of a DS system, a selected list of it's cameras, and a selected list of DS events.

Summary
---------------------
With DSAlarmManager you can configure a rule to make an HTTP request with the body of the message being completely configurable which allows you to send XML, JSON whatever you'd like to whichever IP you'd like. You can also create a rule to trip a relay on the Advantech Adam-6060 model and effectively have relay outputs based on rules.

Features
---------------------
* Configure rules, (events only triggered if a rule is passed)
* Receive notification from DS as relay triggers or HTTP requests.
* Rules persisted on service restart
* Small. < 3MB installer
* Installs and uninstalls cleanly

Video Demos
---------------------
* [Web Service](https://copy.com/qsWjm0cDoUlr)
* [Adam](https://copy.com/tOboVNAcqeKX)

Download
---------------------
[Latest Version](https://bitbucket.org/blackey02/dsalarmmanager/downloads)

Building
---------------------
1. `git clone https://bitbucket.org/blackey02/dsalarmmanager.git`
2. Open the Visual Studio 2013 solution file and build the application
3. The installer project automatically builds everything you need to deploy

Dependencies
---------------------
* .NET 4.5.1
* Pelco DSConnex SDK (7.0.70)

Screens
---------------------
![](https://copy.com/CNdclYZdw94C)
![](https://copy.com/EViHRhF14woc)
![](https://copy.com/3aCvYWNh8F3Y)
![](https://copy.com/ZIf0WUHyRA3r)

Acknowledgements
---------------------
* [Pelco](http://pdn.pelco.com)
* [Advantech](http://www.advantech.com)
* [ServiceStack](https://github.com/ServiceStack/ServiceStack.Text)
* [Log4Net](http://logging.apache.org/log4net/)
* [RestSharp](http://restsharp.org)
* [Effortless.Net.Encryption](https://effortlessencryption.codeplex.com)