﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace DSAlarmManager
{
    public partial class DSAlarmManagerSvc : ServiceBase
    {
        public DSAlarmManagerSvc()
        {
            InitializeComponent();
        }

        public void DebugStart()
        {
            DSAlarmShared.Log.Logger.Instance.Info("DebugStart");
            OnStart(null);
            System.Threading.Thread.Sleep(1000 * 60 * 60);
        }

        protected override void OnStart(string[] args)
        {
            DSAlarmShared.Log.Logger.Instance.Info("OnStart");
            NotificationMgr.Instance.Startup();
            Task.Run(() => Listener.Instance.Start());
        }

        protected override void OnStop()
        {
            DSAlarmShared.Log.Logger.Instance.Info("OnStop");
            NotificationMgr.Instance.Shutdown();
        }
    }
}
