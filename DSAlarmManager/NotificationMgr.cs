﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSAlarmManager
{
    class NotificationMgr
    {
        public class Notification
        {
            public DSAlarmShared.Observer.EventType RecorderEventType { get; set; }
            public int CameraId { get; set; }
            public string AlarmDescription { get; set; }
            public bool IsAlarmed { get; set; }
            public DateTime Timestamp { get; set; }
            public string SystemName { get; set; }
        }

        /// ////////////////////////////////////////////////////////////////////////////////

        private class NotificationObject : IDisposable
        {
            public DSAlarmShared.Rule Rule { get; set; }
            public DSSdk.System System { get; set; }

            public void Dispose()
            {
                if (System != null)
                {
                    System.Disconnect();
                }
                GC.SuppressFinalize(this);
            }
        }

        /// ////////////////////////////////////////////////////////////////////////////////

        private static NotificationMgr _instance = null;
        private static readonly object _instanceLock = new object();

        public static NotificationMgr Instance
        {
            get
            {
                lock (_instanceLock)
                {
                    if (_instance == null)
                        _instance = new NotificationMgr();
                    return _instance;
                }
            }
        }

        private Dictionary<string, NotificationObject> _notObjects = new Dictionary<string, NotificationObject>();
        private DataLocker<List<DSAlarmShared.Rule>> _ruleLocker = new DataLocker<List<DSAlarmShared.Rule>>("rules");

        private NotificationMgr()
        { }

        public List<DSAlarmShared.Rule> Rules()
        {
            List<DSAlarmShared.Rule> rules;
            lock (_notObjects)
            {
                rules = _notObjects.Values.Select(notObject => notObject.Rule).ToList().ConvertAll(rule => new DSAlarmShared.Rule(rule));
            }
            return rules;
        } 

        public void Startup()
        {
            var rules = _ruleLocker.LoadData();
            Parallel.ForEach(rules, rule => AddRule(rule, false));
            SaveRules();
        }

        public void Shutdown()
        {
            lock (_notObjects)
            {
                SaveRules();
                foreach (var notObject in _notObjects.Values)
                    notObject.Dispose();
                _notObjects.Clear();
            }
        }

        public void IncomingAlarm(Notification notification)
        {
            var rules = Rules();
            var filteredRules = rules.Where(rule => rule.Cameras.Any(cam => cam.CameraID == notification.CameraId));
            
            Parallel.ForEach(filteredRules, filteredRule =>
            {
                var sharedNotification = new DSAlarmShared.Observer.Notification();
                sharedNotification.Camera = filteredRule.Cameras.Where(camera => camera.CameraID == notification.CameraId).FirstOrDefault();
                sharedNotification.System = filteredRule.System;
                sharedNotification.System.Name = notification.SystemName ?? sharedNotification.System.Name;
                sharedNotification.AlarmDescription = notification.AlarmDescription;
                sharedNotification.IsAlarmed = notification.IsAlarmed;
                sharedNotification.RecorderEventType = notification.RecorderEventType;
                sharedNotification.Timestamp = notification.Timestamp;

                bool shouldEvent = (filteredRule.Observer.EventsObserved & sharedNotification.RecorderEventType) == sharedNotification.RecorderEventType;

                if (shouldEvent)
                    filteredRule.Observer.OnEvent(sharedNotification);
            });
        }

        private void SaveRules()
        {
            var rules = Rules();
            _ruleLocker.SaveData(rules);
        }

        public void AddRule(DSAlarmShared.Rule rule, bool save = true)
        {
            var notObject = new NotificationObject();
            notObject.Rule = rule;
            notObject.System = new DSSdk.System(rule.System);

            bool isConnected = notObject.System.Connect();
            if (isConnected)
            {
                bool newCamerasHaveSubscribed = notObject.System.Subscribe(rule.Cameras);
                lock (_notObjects)
                {
                    if (_notObjects.ContainsKey(rule.Id))
                        _notObjects[rule.Id] = notObject;
                    else
                        _notObjects.Add(rule.Id, notObject);
                }
            }
            else
                notObject.Dispose();

            if(save)
                SaveRules();
            
            System.Diagnostics.Debug.WriteLine("====== Rule Added");
        }

        public void RemoveRule(string id)
        {
            lock (_notObjects)
            {
                if (_notObjects.ContainsKey(id))
                {
                    var notObject = _notObjects[id];
                    notObject.Dispose();
                    _notObjects.Remove(id);
                }
            }
            SaveRules();
        }
    }
}
