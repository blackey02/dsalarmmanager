﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace DSAlarmManager
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        ServiceController _service;

        public ProjectInstaller()
        {
            InitializeComponent();
            _service = new ServiceController(this.serviceInstaller.ServiceName);
        }

        protected override void OnAfterInstall(IDictionary savedState)
        {
            try
            {
                _service.Start();
                _service.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(30));
            }
            catch (Exception e)
            {
                string msg = DSAlarmShared.Log.Logger.Instance.FormatObject("OnAfterInstall", false);
                DSAlarmShared.Log.Logger.Instance.Warn(msg, e);
            }

            base.OnAfterInstall(savedState);
        }
    }
}
