﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Integral.Client.Sources;

namespace DSAlarmManager.DSSdk
{
    class System
    {
        private class SystemRefTracker
        {
            public int Refs { get; set; }
            public List<DSAlarmShared.DS.Camera> SubscribedCams { get; set; }

            public SystemRefTracker()
            {
                Refs = 0;
                SubscribedCams = new List<DSAlarmShared.DS.Camera>();
            }
        }

        /// ////////////////////////////////////////////////////////////////////////////////

        private static Dictionary<string, SystemRefTracker> Refs = new Dictionary<string, SystemRefTracker>();
        private DSAlarmShared.DS.System _system;
        private IDVRSystem _dsDvr;
        private bool _isConnected;
        private IDataChannel _dataChannel;
        private AlarmHandler _alarmHandler;
        
        public System(DSAlarmShared.DS.System system)
        {
            _system = system;

            lock (Refs)
            {
                if (!Refs.ContainsKey(SystemId))
                    Refs.Add(SystemId, new SystemRefTracker());
            }
        }

        public string LastError 
        { 
            get 
            {
                string lastError = string.Empty;
                if (_dsDvr != null)
                    lastError = _dsDvr.LastError;
                return lastError;
            } 
        }

        public string SystemId
        {
            get
            {
                return string.Format("{0}|{1}|{2}", _system.Ip, _system.Port, _system.Username);
            }
        }

        public bool Connect()
        {
            if (_isConnected)
                return true;

            lock (Refs)
            {
                _dsDvr = SystemFactory.FindOrCreateSystem<IDVRSystem>(_system.Ip, _system.Port, _system.Username, _system.Password, SystemType.DigitalSENTRY);
                Refs[SystemId].Refs++;
            }

            _dsDvr.Open();
            _isConnected = _dsDvr.Recorders.Count > 0;

            string msg = DSAlarmShared.Log.Logger.Instance.FormatObject("Connect", _system, _isConnected);
            DSAlarmShared.Log.Logger.Instance.Info(msg);

            return _isConnected;
        }

        public void Disconnect()
        {
            lock (Refs)
            {
                Refs[SystemId].Refs--;

                if (Refs[SystemId].Refs == 0 && _dsDvr != null)
                {
                    _dsDvr.Close();
                    _dsDvr = null;
                    Refs[SystemId].SubscribedCams.Clear();
                }
            }
            _isConnected = false;
        }

        public List<DSAlarmShared.DS.Camera> Cameras()
        {
            var cameras = new List<DSAlarmShared.DS.Camera>();
            if (!_isConnected)
                return cameras;

            foreach (var recorder in _dsDvr.Recorders)
            {
                foreach (var camera in recorder.Cameras)
                {
                    var sharedCam = new DSAlarmShared.DS.Camera();
                    sharedCam.FriendlyName = camera.Name;
                    sharedCam.CameraID = camera.CameraID;
                    sharedCam.Detected = camera.Detected == 1;
                    sharedCam.IsIpCamera = camera.IsIpCamera;
                    sharedCam.IsPtz = camera.PTZControl != null;
                    sharedCam.Number = camera.Number;
                    cameras.Add(sharedCam);
                }
            }
            return cameras;
        }

        public bool Subscribe(List<DSAlarmShared.DS.Camera> cameras)
        {
            if (!_isConnected)
                return false;

            List<DSAlarmShared.DS.Camera> camsToSubscribe;
            lock(Refs[SystemId].SubscribedCams)
            {
                var camIds = Refs[SystemId].SubscribedCams.Select(cam => cam.CameraID);
                camsToSubscribe = cameras.Where(cam => !camIds.Contains(cam.CameraID)).ToList();
                Refs[SystemId].SubscribedCams.AddRange(camsToSubscribe);
            }

            var container = new Dictionary<string,dynamic>();
            foreach (var camera in camsToSubscribe)
            {
                ICamera dsCamera = null;
                IRecorder recorder = _dsDvr.Recorders.Find(rec => (dsCamera = rec.Cameras.Where(cam => cam.CameraID == camera.CameraID && cam.Name == camera.FriendlyName).FirstOrDefault()) != null);
                if (recorder != null)
                {
                    if (!container.ContainsKey(recorder.IPAddress))
                    {
                        var containerItem = new { Recorder = recorder, Cameras = new CameraCollection() };
                        container.Add(recorder.IPAddress, containerItem);
                    }
                    container[recorder.IPAddress].Cameras.Add(dsCamera);
                }
            }

            foreach (var containerItem in container.Values)
            {
                var itemRrecorder = containerItem.Recorder;
                var itemCameras = containerItem.Cameras;

                _dataChannel = itemRrecorder.DataChannels[0];
                _alarmHandler = new AlarmHandler();
                RenderParameter parameter = new RenderParameter(_alarmHandler);
                RecorderEventType events = RecorderEventType.EnableAll;
                // Listen for all alarm types
                parameter.AddParameter(RenderParameter.DATA_Filter, events);
                // Enable motion events for all cameras
                parameter.AddParameter(RenderParameter.DATA_MotionCameraFilter, itemCameras);
                _dataChannel.Render(parameter);
            }

            return container.Count > 0;
        }

        private RecorderEventType ConvertEvents(DSAlarmShared.Observer.EventType eventType)
        {
            if ((eventType & DSAlarmShared.Observer.EventType.All) == DSAlarmShared.Observer.EventType.All)
                return RecorderEventType.EnableAll;

            RecorderEventType dsEventType = RecorderEventType.DisableAll;
            if ((eventType & DSAlarmShared.Observer.EventType.Hard) == DSAlarmShared.Observer.EventType.Hard)
                dsEventType |= RecorderEventType.HardAlarm;
            if ((eventType & DSAlarmShared.Observer.EventType.Manual) == DSAlarmShared.Observer.EventType.Manual)
                dsEventType |= RecorderEventType.Manual;
            if ((eventType & DSAlarmShared.Observer.EventType.Motion) == DSAlarmShared.Observer.EventType.Motion)
                dsEventType |= RecorderEventType.Motion;
            if ((eventType & DSAlarmShared.Observer.EventType.Serial) == DSAlarmShared.Observer.EventType.Serial)
                dsEventType |= RecorderEventType.Serial;
            if ((eventType & DSAlarmShared.Observer.EventType.Soft) == DSAlarmShared.Observer.EventType.Soft)
                dsEventType |= RecorderEventType.SoftAlarm;
            if ((eventType & DSAlarmShared.Observer.EventType.Trigger) == DSAlarmShared.Observer.EventType.Trigger)
                dsEventType |= RecorderEventType.Trigger;
            if ((eventType & DSAlarmShared.Observer.EventType.Unknown) == DSAlarmShared.Observer.EventType.Unknown)
                dsEventType |= RecorderEventType.Unknown;
            if ((eventType & DSAlarmShared.Observer.EventType.VideoLoss) == DSAlarmShared.Observer.EventType.VideoLoss)
                dsEventType |= RecorderEventType.VideoLoss;
            return dsEventType;
        }
    }
}
