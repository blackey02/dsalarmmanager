﻿using Integral.Client.Sources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DSAlarmManager.DSSdk
{
    class AlarmHandler : IDataTransactionItemHandler
    {
        public void HandleClientAction(DataTransactionItem action)
        { }

        public void OnCurrentIndexChanged(DataTransactionItemCollection list, int currentIndex)
        { }

        public void OnListChanged(DataTransactionItemCollection list, global::System.ComponentModel.ListChangedEventArgs e)
        {
            if (list == null)
            {
                string msg = DSAlarmShared.Log.Logger.Instance.FormatObject("OnListChanged", false, "Ignoring incoming list of events, list of events is null");
                DSAlarmShared.Log.Logger.Instance.Warn(msg);
                return;
            }

            foreach (var item in list)
            {
                if (item == null)
                {
                    string msg = DSAlarmShared.Log.Logger.Instance.FormatObject("OnListChanged", false, "Ignoring event, event is null");
                    DSAlarmShared.Log.Logger.Instance.Warn(msg);
                    continue;
                }

                var recorder = item.Channel.Source as IRecorder;
                if (recorder == null)
                {
                    string msg = DSAlarmShared.Log.Logger.Instance.FormatObject("OnListChanged", false, "Ignoring event, event channel's source is not an IRecorder");
                    DSAlarmShared.Log.Logger.Instance.Warn(msg);
                    continue;
                }

                IDVRSystem dvr = recorder.DVRSystem;
                if (dvr != null && !dvr.UserRights.HasRight("Alarm Activity"))
                {
                    string msg = DSAlarmShared.Log.Logger.Instance.FormatObject("OnListChanged", false, string.Format("Ignoring event, {0} is not authorized to view alarm activity", dvr.Username));
                    DSAlarmShared.Log.Logger.Instance.Warn(msg);
                    continue;
                }

                HandleDataTransactionItem(item, e.ListChangedType);
            }
            list.Clear();
        }

        private void HandleDataTransactionItem(DataTransactionItem item, ListChangedType changeType)
        {
            string type;
            bool success = item.TryGetValue("RecorderEventType", out type);
            string cameraName;
            success = item.TryGetValue("CameraName", out cameraName);
            int cameraNumber = int.Parse(item["CameraNumber"]);
            string alarmDescription = item.Text;
            bool isAlarmed = item.Alarmed;
            DateTime timestamp = item.TimeStamp;
            ICamera sourceCam = item.Channel.Source.Cameras.FindByNumber(cameraNumber);

            if (sourceCam == null)
            {
                string msg = DSAlarmShared.Log.Logger.Instance.FormatObject("HandleDataTransactionItem", false, "Ignoring event, unable to find camera associated with event");
                DSAlarmShared.Log.Logger.Instance.Warn(msg);
                return;
            }

            int cameraId = sourceCam.CameraID;

            // Serial alarms don't have an end so set our alarmed variable to false
            // to treat it like it's the beginning and end
            bool setAlarmEnded = false;
            setAlarmEnded = setAlarmEnded || type == RecorderEventType.Serial.ToString();
            if (setAlarmEnded)
                isAlarmed = false;

            // If this was a trigger, and we don't have the camera name then try to get it
            string triggerNumber;
            if (string.IsNullOrWhiteSpace(cameraName) && item.TryGetValue("Trigger", out triggerNumber))
            {
                int triggerNum;
                if (int.TryParse(triggerNumber, out triggerNum) && triggerNum >= 0)
                {
                    ICamera cam = item.Channel.Source.Cameras.FindByNumber(triggerNum);
                    if (cam != null)
                        cameraName = cam.Name;
                }
            }

            var notification = new NotificationMgr.Notification();
            notification.RecorderEventType = ConvertEvents(type);
            notification.CameraId = cameraId;
            notification.AlarmDescription = alarmDescription;
            notification.IsAlarmed = isAlarmed;
            notification.Timestamp = timestamp;
            if (item.Channel.Source != null && item.Channel.Source is IRecorder)
            {
                IDVRSystem dvr = (item.Channel.Source as IRecorder).DVRSystem;
                notification.SystemName = dvr.Name;
            }

            NotificationMgr.Instance.IncomingAlarm(notification);
        }

        private DSAlarmShared.Observer.EventType ConvertEvents(string recorderEventType)
        {
            DSAlarmShared.Observer.EventType sharedEvent = DSAlarmShared.Observer.EventType.NotSpecified;
            if (((RecorderEventType)Enum.Parse(typeof(RecorderEventType), recorderEventType) & RecorderEventType.HardAlarm) == RecorderEventType.HardAlarm)
                sharedEvent |= DSAlarmShared.Observer.EventType.Hard;
            if (((RecorderEventType)Enum.Parse(typeof(RecorderEventType), recorderEventType) & RecorderEventType.Manual) == RecorderEventType.Manual)
                sharedEvent |= DSAlarmShared.Observer.EventType.Manual;
            if (((RecorderEventType)Enum.Parse(typeof(RecorderEventType), recorderEventType) & RecorderEventType.Motion) == RecorderEventType.Motion)
                sharedEvent |= DSAlarmShared.Observer.EventType.Motion;
            if (((RecorderEventType)Enum.Parse(typeof(RecorderEventType), recorderEventType) & RecorderEventType.Serial) == RecorderEventType.Serial)
                sharedEvent |= DSAlarmShared.Observer.EventType.Serial;
            if (((RecorderEventType)Enum.Parse(typeof(RecorderEventType), recorderEventType) & RecorderEventType.SoftAlarm) == RecorderEventType.SoftAlarm)
                sharedEvent |= DSAlarmShared.Observer.EventType.Soft;
            if (((RecorderEventType)Enum.Parse(typeof(RecorderEventType), recorderEventType) & RecorderEventType.Trigger) == RecorderEventType.Trigger)
                sharedEvent |= DSAlarmShared.Observer.EventType.Trigger;
            if (((RecorderEventType)Enum.Parse(typeof(RecorderEventType), recorderEventType) & RecorderEventType.Unknown) == RecorderEventType.Unknown)
                sharedEvent |= DSAlarmShared.Observer.EventType.Unknown;
            if (((RecorderEventType)Enum.Parse(typeof(RecorderEventType), recorderEventType) & RecorderEventType.VideoLoss) == RecorderEventType.VideoLoss)
                sharedEvent |= DSAlarmShared.Observer.EventType.VideoLoss;
            return sharedEvent;
        }
    }
}
