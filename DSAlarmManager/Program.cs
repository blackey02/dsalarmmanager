﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace DSAlarmManager
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            if (Environment.GetCommandLineArgs().Any(arg => arg.ToLowerInvariant() == "debug"))
            {
                var svc = new DSAlarmManagerSvc();
                svc.DebugStart();
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
                { 
                    new DSAlarmManagerSvc() 
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
