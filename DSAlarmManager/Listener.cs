﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace DSAlarmManager
{
    class Listener
    {
        private static Listener _instance = null;
        private static readonly object _instanceLock = new object();

        public static Listener Instance
        {
            get
            {
                lock (_instanceLock)
                {
                    if (_instance == null)
                        _instance = new Listener();
                    return _instance;
                }
            }
        }

        private DSAlarmShared.Pipe.Server _server = null;

        private Listener()
        { }

        public void Start()
        {
            if (_server == null)
            {
                _server = new DSAlarmShared.Pipe.Server(DSAlarmShared.Constants.PipeName);
                _server.ClientConnected += new Action<int>(ClientConnected);
                _server.WaitForClientsAsync();
            }
        }

        private void ClientConnected(int id)
        {
            DSAlarmShared.Pipe.Request request = null;
            do
            {
                var response = new DSAlarmShared.Pipe.Response();
                request = _server.Receive<DSAlarmShared.Pipe.Request>(id);
                if (request != null)
                {
                    string msg = DSAlarmShared.Log.Logger.Instance.FormatObject("ClientConnected", true, 
                        string.Format("Request: {0}", Enum.GetName(typeof(DSAlarmShared.Pipe.Request.PipeAction), request.Action)));
                    DSAlarmShared.Log.Logger.Instance.Info(msg);

                    switch (request.Action)
                    {
                        case DSAlarmShared.Pipe.Request.PipeAction.GetCameras:
                            {
                                var cameras = new List<DSAlarmShared.DS.Camera>();
                                var sharedSystem = request.Data as DSAlarmShared.DS.System;
                                if (sharedSystem != null)
                                {
                                    var system = new DSSdk.System(sharedSystem);
                                    bool success = system.Connect();
                                    if (success)
                                    {
                                        cameras = system.Cameras();
                                        system.Disconnect();
                                        response.Success = true;
                                    }
                                }
                                response.Data = cameras;
                                _server.Send(id, response);
                                break;
                            }
                        case DSAlarmShared.Pipe.Request.PipeAction.AddRule:
                            {
                                var rule = request.Data as DSAlarmShared.Rule;
                                if (rule != null)
                                {
                                    NotificationMgr.Instance.AddRule(rule);
                                    response.Success = true;
                                }
                                _server.Send(id, response);
                                break;
                            }
                        case DSAlarmShared.Pipe.Request.PipeAction.GetRules:
                            {
                                var rules = NotificationMgr.Instance.Rules();
                                response.Success = true;
                                response.Data = rules;
                                _server.Send(id, response);
                                break;
                            }
                        case DSAlarmShared.Pipe.Request.PipeAction.RemoveRule:
                            {
                                var rule = request.Data as DSAlarmShared.Rule;
                                if (rule != null)
                                {
                                    NotificationMgr.Instance.RemoveRule(rule.Id);
                                    response.Success = true;
                                }
                                _server.Send(id, response);
                                break;
                            }
                        default:
                            break;
                    }
                }
            }
            while (request != null && (request.Option & DSAlarmShared.Pipe.Request.PipeOption.KeepOpen) == DSAlarmShared.Pipe.Request.PipeOption.KeepOpen);
            _server.DisconnectClient(id);
        }
    }
}
