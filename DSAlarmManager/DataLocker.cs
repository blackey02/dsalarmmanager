﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Text;

namespace DSAlarmManager
{
    class DataLocker<T> where T : new()
    {
        [Serializable]
        private class LockerItem<To>
        {
            public To Data { get; private set; }
            public byte[] IV { get; private set; }
            public Version Ver { get; private set; }

            public LockerItem(To data, byte[] iv)
            {
                Ver = new Version(1, 0, 0);
                Data = data;
                IV = iv;
            }
        }

        /// ////////////////////////////////////////////////////////////////////////////////

        private const string SuprSecret = @"|-|4\/34B33r0|\|/\/\3";
        private readonly object _fileLock = new object();
        private string _fileName;

        public DataLocker(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                throw new NullReferenceException("fileName");
            _fileName = fileName;
        }

        private string LockerFile
        {
            get
            {
                var baseDir = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
                    DSAlarmShared.Constants.ApplicationName);
                if (!Directory.Exists(baseDir))
                    Directory.CreateDirectory(baseDir);
                string lockerFile = Path.Combine(baseDir, _fileName + ".dat");
                return lockerFile;
            }
        }

        private string ReadLockerFile()
        {
            lock (_fileLock)
            {
                return File.ReadAllText(LockerFile);
            }
        }

        private void WriteLockerFile(string data)
        {
            lock (_fileLock)
            {
                File.WriteAllText(LockerFile, data);
            }
        }

        public void SaveData(T data)
        {
            // Get IV if locker file exists
            var serializer = new ServiceStack.Text.JsonStringSerializer();
            byte[] iv = Effortless.Net.Encryption.Bytes.GenerateIV();
            if (File.Exists(LockerFile))
                iv = serializer.DeserializeFromString<LockerItem<string>>(ReadLockerFile()).IV;

            // Serialize & encrypt data
            var strData = serializer.SerializeToString(data);
            byte[] key = Effortless.Net.Encryption.Bytes.GenerateKey(SuprSecret, SuprSecret, Effortless.Net.Encryption.Bytes.KeySize.Size256);
            var encryptedStrData = Effortless.Net.Encryption.Strings.Encrypt(strData, key, iv);
            var lockerItem = new LockerItem<string>(encryptedStrData, iv);
            var json = serializer.SerializeToString(lockerItem);

            // Write rules to locker file
            WriteLockerFile(json);
        }

        public T LoadData()
        {
            var data = new T();
            if(File.Exists(LockerFile))
            {
                // Read rules from cache
                string dataStr = ReadLockerFile();

                // Deserialize & decrypt data
                var deserializer = new ServiceStack.Text.JsonStringSerializer();
                var lockerItem = deserializer.DeserializeFromString<LockerItem<string>>(dataStr);
                
                byte[] key = Effortless.Net.Encryption.Bytes.GenerateKey(SuprSecret, SuprSecret, Effortless.Net.Encryption.Bytes.KeySize.Size256);
                byte[] iv = lockerItem.IV;

                var decryptedDataStr = Effortless.Net.Encryption.Strings.Decrypt(lockerItem.Data, key, iv);
                data = deserializer.DeserializeFromString<T>(decryptedDataStr);
            }
            return data;
        }
    }
}
