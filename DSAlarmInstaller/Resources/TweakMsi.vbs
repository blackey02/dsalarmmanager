Dim installer, database, view, componentView, componentId

Set installer = CreateObject("WindowsInstaller.Installer")
Set database = installer.OpenDatabase (WScript.Arguments(0), 1)

Set componentView = database.OpenView("SELECT Component_ FROM File WHERE FileName = 'DSALAR~2.EXE|DSAlarmManager.exe'")
componentView.Execute
componentId = componentView.Fetch.StringData(1)

Set view = database.OpenView("INSERT INTO ServiceControl (ServiceControl,Name,Event,Component_) VALUES ('DSAlarmManager','DSAlarmManager',42,'" & componentId & "')") 
view.Execute
database.Commit

Set database = nothing

